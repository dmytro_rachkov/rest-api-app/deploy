#!/usr/bin/env python
import json
import sys
import requests

from inputs.parser import transform_csv

csv_file = "inputs/input-test-data.csv"


def create_users(url_arg, method_arg):
    c = transform_csv(csv_file)
    data = json.loads(c)
    for item in data:
        user = {
            "id": int(item["id"]),
            "name": str(item["name"])
        }
        headers = {'Content-type': 'application/json'}
        r = requests.post(url=url_arg, data=json.dumps(user), headers=headers)
        code = r.status_code
    return code


def delete_users(url_arg, method_arg):
    d = transform_csv(csv_file)
    data = json.loads(d)
    for item in data:
        headers = {'Content-type': 'application/json'}
        r = requests.delete(url=url_arg + str(item["id"]), headers=headers)
        code = r.status_code
    return code


def main():
    if method_arg == "POST":
        c = create_users(url_arg, method_arg)
        print(c)
        if c != 200:
            raise Exception('Creation of users has failed with http code', c)
    elif method_arg == "DELETE":
        d = delete_users(url_arg, method_arg)
        print(d)
        if d != 200:
            raise Exception('Deletion of users has failed with http code', d)


if __name__ == '__main__':
    if len(sys.argv) < 3:
        raise Exception('Program cannot be run without 2 arguments: url and'
                        'HTTP method')
    else:
        url_arg = sys.argv[1]
        method_arg = sys.argv[2]
        main()
